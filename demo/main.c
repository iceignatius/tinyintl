#ifdef NDEBUG
#   undef NDEBUG                // To force enable assert.
#endif

#include <assert.h>             // To use assert, For test use.
#include <stdlib.h>             // To use getenv, setenv.
#include <stdio.h>              // To use printf.
#include <locale.h>             // To use setlocale.
#ifdef _WIN32
#   include <wchar.h>           // To use getcwd, for test use.
#   include <utfconv/utfconv.h> // To use UTF-WCS converter.
#else
#   include <unistd.h>          // To use getcwd, for test use.
#endif
#include <tinyintl.h>           // To use bindtextdomain, textdomain, and gettext.

// Tips: Use this define to short the "gettext" statement.
#define _(str) tinyintl_gettext(str)

int main(int argc, char *argv[])
{
    printf("---- setting environments begin ----\n");

    /*
     * First, calculate the locale resource path.
     * This is designed for "testing in any path by any user" purpose
     * and may not be needed for the real application.
     * Normal applications usually use the absolute path, for example:
     * /usr/share/locale
     */

#ifdef _WIN32
    wchar_t wcscwd[FILENAME_MAX];
    assert(_wgetcwd(wcscwd, FILENAME_MAX));

    char cwd[FILENAME_MAX];
    assert(utfconv_wcs_to_utf8(cwd, sizeof(cwd), wcscwd));
#else
    char cwd[FILENAME_MAX];
    assert(getcwd(cwd, sizeof(cwd)));
#endif
    printf("CWD: %s\n", cwd);

    char locale_dir[FILENAME_MAX];
    snprintf(locale_dir, sizeof(locale_dir), "%s/%s", cwd, "locale");
    printf("Calculate locale DIR: %s\n", locale_dir);

    // Setting locale.
#ifdef _WIN32
    // Try to get user value first.
    const char *locale_name = getenv("LC_ALL");
    // If failed, then using the system locale.
    locale_name = tinyintl_setlocale(LC_ALL, locale_name ? locale_name : "");
#else
    // Using system locale setting.
    tinyintl_setlocale(LC_ALL, "");
    const char *locale_name = tinyintl_setlocale(LC_MESSAGES, NULL);
#endif
    printf("Set locale: %s\n", locale_name);
    assert(locale_name);

    // Setting language environment variable.
    setenv("LANGUAGE", locale_name, 1);

    // Define the domain name.
    // It is recommended to use the application name.
    static const char *domain = "gettext-demo";

    // Load the locale resource file.
    // Which file will be loaded is:
    // <LOCALE-DIR>/<LANGUAGE>[_<COUNTRY>]/<CATEGORY>/<DOMAIN>.mo
    // for example:
    // /usr/share/locale/zh_TW/LC_MESSAGES/vlc.mo
    const char *curr_domain = tinyintl_textdomain(domain);
    printf("Set domain: %s\n", curr_domain);    // The domain can be NULL to
                                                // use the previous setting.
    assert(curr_domain);

    // Locate the path of locale resource files.
    const char *binded_dir = tinyintl_bindtextdomain(domain, locale_dir);
    printf("Bind DIR: %s\n", binded_dir);
    assert(binded_dir);

    printf("---- setting environments end ----\n");
    printf("\n");

    /*
     * Now start using gettext functions.
     * Every strings which be embraced by "gettext"
     * will be marked then try to translated.
     */

    const char *message = tinyintl_gettext("This is a simple message.");
    printf("%s\n", message);

    //+ This is a special comment that will be output to the POT file.
    printf(_("I will go to %s by %s.\n"), _("Taipei"), _("bus"));

    return 0;
}
