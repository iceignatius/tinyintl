# The Tiny International Library

"tinyintl" is a simple implementation of "libintl" library.

Just use tinyintl like libintl, except:
* Call `libintl_setlocale` with `LL_CC` format of locale
    instead of calling `setlocale`.
* Link `intl` library.
* The "tinyintl" will not convert text encoding.
