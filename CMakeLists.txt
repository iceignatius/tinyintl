cmake_minimum_required(VERSION 3.5)
project(tinyintl)

if(WIN32)
    find_library(HAVE_UTFCONV utfconv)
    message("utfconv: ${HAVE_UTFCONV}")
    if(NOT HAVE_UTFCONV)
        message(FATAL_ERROR "Mandatory dependency: utfconv!")
    endif()
endif()

add_subdirectory(lib)
add_subdirectory(demo)
