#ifndef _TINYINTL_MOFILE_H_
#define _TINYINTL_MOFILE_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(push,1)
struct tinyintl_moheader
{
    uint32_t magic;
    uint32_t version;
    uint32_t string_num;
    uint32_t original_offset;
    uint32_t translated_offset;
    uint32_t hash_num;
    uint32_t hash_offset;
};
#pragma pack(pop)

typedef struct tinyintl_mofile
{
    char *alldata;
    size_t allsize;

    struct tinyintl_moheader *header;
    uint32_t *hash_table;

    char **original_table;
    char **translated_table;

} tinyintl_mofile_t;

#define TINYINTL_MOFILE_INITIALISER {0}

void tinyintl_mofile_init(tinyintl_mofile_t *self);
void tinyintl_mofile_destroy(tinyintl_mofile_t *self);

bool tinyintl_mofile_load_file(tinyintl_mofile_t *self, const char *filename);
void tinyintl_mofile_clear(tinyintl_mofile_t *self);
bool tinyintl_mofile_is_loaded(const tinyintl_mofile_t *self);

unsigned tinyintl_mofile_count_string(const tinyintl_mofile_t *self);
const char* tinyintl_mofile_get_original(
    const tinyintl_mofile_t *self, unsigned index);
const char* tinyintl_mofile_get_translated(
    const tinyintl_mofile_t *self, unsigned index);
const char* tinyintl_mofile_find_translated(
    const tinyintl_mofile_t *self, const char *original);

unsigned tinyintl_mofile_count_hash(const tinyintl_mofile_t *self);
uint32_t tinyintl_mofile_get_hash(const tinyintl_mofile_t *self, unsigned index);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
