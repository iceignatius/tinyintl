#ifndef _LIBINTL_H_
#define _LIBINTL_H_

#include "tinyintl.h"

#define bindtextdomain(domain, dir) tinyintl_bindtextdomain(domain, dir)
#define textdomain(domain)          tinyintl_textdomain(domain)
#define gettext(msgid)              tinyintl_gettext(msgid)

#endif
