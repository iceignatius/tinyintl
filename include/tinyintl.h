#ifndef _TINYINTL_H_
#define _TINYINTL_H_

#ifdef __cplusplus
extern "C" {
#endif

char* tinyintl_setlocale(int category, const char *locale);
char* tinyintl_bindtextdomain(const char *domainname, const char *dirname);
char* tinyintl_textdomain(const char *domainname);
char* tinyintl_gettext(const char *msgid);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
