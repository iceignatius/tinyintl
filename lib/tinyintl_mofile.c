#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
#   include <utfconv/utfconv.h>
#endif

#include "tinyintl_mofile.h"

#pragma pack(push,1)
struct string_table_item
{
    uint32_t length;
    uint32_t offset;
};
#pragma pack(pop)

void tinyintl_mofile_init(tinyintl_mofile_t *self)
{
    memset(self, 0, sizeof(*self));
}

void tinyintl_mofile_destroy(tinyintl_mofile_t *self)
{
    tinyintl_mofile_clear(self);
}

static
char* load_file_data(const char *filename, size_t *filesize)
{
    char *data = NULL;
    long size;

    FILE *file = NULL;

    bool succ = false;
    do
    {
#ifdef _WIN32
        if( !( file = utfconv_fopen(filename, "rb") ) )
            break;
#else
        if( !( file = fopen(filename, "rb") ) )
            break;
#endif
        if(fseek(file, 0, SEEK_END))
            break;
        if( 0 > ( size = ftell(file) ) )
            break;

        if( !( data = malloc(size) ) )
            break;

        rewind(file);
        if( size != fread(data, 1, size, file) )
            break;

        *filesize = size;

        succ = true;
    } while(false);

    if(file)
        fclose(file);

    if( !succ && data )
    {
        free(data);
        data = NULL;
    }

    return data;
}

static
uint32_t swap_word(uint32_t val)
{
    val = ( ( val << 8 )&0xFF00FF00 )|( ( val >> 8 )&0x00FF00FF );
    return ( val << 16 )|( val >> 16 );
}

static
struct tinyintl_moheader* read_header(char *data, size_t size, bool *needswap)
{
    struct tinyintl_moheader *header = (struct tinyintl_moheader*) data;
    if( !data || size < sizeof(*header) )
        return NULL;

    static const uint32_t magic = 0x950412DE;
    static const uint32_t magic_swapped = 0xDE120495;

    bool need_swap;
    if( header->magic == magic )
        need_swap = *needswap = false;
    else if( header->magic == magic_swapped )
        need_swap = *needswap = true;
    else
        return NULL;

    if(need_swap)
    {
        header->magic = swap_word(header->magic);
        header->version = swap_word(header->version);
        header->string_num = swap_word(header->string_num);
        header->original_offset = swap_word(header->original_offset);
        header->translated_offset = swap_word(header->translated_offset);
        header->hash_num = swap_word(header->hash_num);
        header->hash_offset = swap_word(header->hash_offset);
    }

    uint16_t major_version = header->version >> 16;
    if( major_version > 1 )
        return NULL;

    size_t table_size = header->string_num * sizeof(struct string_table_item);
    if( header->original_offset + table_size > size )
        return NULL;
    if( header->translated_offset + table_size > size )
        return NULL;

    size_t hash_size = header->hash_num * sizeof(uint32_t);
    if( header->hash_offset + hash_size > size )
        return NULL;

    return header;
}

static
uint32_t* read_hash_table(
    char *data, size_t size, size_t offset, unsigned num, bool needswap)
{
    uint32_t *table = (uint32_t*)( data + offset );

    size_t table_size = num * sizeof(table[0]);
    if( offset + table_size > size )
        return NULL;

    if(needswap)
    {
        for(unsigned i = 0; i < num; ++i)
            table[i] = swap_word(table[i]);
    }

    return table;
}

static
struct string_table_item* read_string_table(
    char *data, size_t size, size_t offset, unsigned num, bool needswap)
{
    struct string_table_item *table =
        (struct string_table_item*)( data + offset );

    size_t table_size = num * sizeof(table[0]);
    if( offset + table_size > size )
        return NULL;

    if(needswap)
    {
        for(unsigned i = 0; i < num; ++i)
        {
            table[i].length = swap_word(table[i].length);
            table[i].offset = swap_word(table[i].offset);
        }
    }

    for(unsigned i = 0; i < num; ++i)
    {
        if( table[i].offset + table[i].length + 1 > size )
            return NULL;
    }

    return table;
}

static
char** create_string_cache_table(
    char *data, const struct string_table_item *table, unsigned num)
{
    char **list = malloc(num * sizeof(char*));
    if(!list) return NULL;

    for(unsigned i = 0; i < num; ++i)
        list[i] = data + table[i].offset;

    return list;
}

bool tinyintl_mofile_load_file(tinyintl_mofile_t *self, const char *filename)
{
    tinyintl_mofile_clear(self);

    bool succ = false;
    do
    {
        if(!filename)
            break;
        if( !( self->alldata = load_file_data(filename, &self->allsize) ) )
            break;

        bool needswap;
        self->header = read_header(self->alldata, self->allsize, &needswap);
        if(!self->header)
            break;

        self->hash_table = read_hash_table(
            self->alldata,
            self->allsize,
            self->header->hash_offset,
            self->header->hash_num,
            needswap);
        if(!self->hash_table)
            break;

        struct string_table_item *original_info_table = read_string_table(
            self->alldata,
            self->allsize,
            self->header->original_offset,
            self->header->string_num,
            needswap);
        if(!original_info_table)
            break;

        self->original_table = create_string_cache_table(
            self->alldata,
            original_info_table,
            self->header->string_num);
        if(!self->original_table)
            break;

        struct string_table_item *translated_info_table = read_string_table(
            self->alldata,
            self->allsize,
            self->header->translated_offset,
            self->header->string_num,
            needswap);
        if(!translated_info_table)
            break;

        self->translated_table = create_string_cache_table(
            self->alldata,
            translated_info_table,
            self->header->string_num);
        if(!self->translated_table)
            break;

        succ = true;
    } while(false);

    if(!succ)
        tinyintl_mofile_clear(self);

    return succ;
}

void tinyintl_mofile_clear(tinyintl_mofile_t *self)
{
    if(self->alldata)
        free(self->alldata);

    if(self->original_table)
        free(self->original_table);

    if(self->translated_table)
        free(self->translated_table);

    memset(self, 0, sizeof(*self));
}

bool tinyintl_mofile_is_loaded(const tinyintl_mofile_t *self)
{
    return !!self->alldata;
}

unsigned tinyintl_mofile_count_string(const tinyintl_mofile_t *self)
{
    return self->header->string_num;
}

const char* tinyintl_mofile_get_original(
    const tinyintl_mofile_t *self, unsigned index)
{
    return index < self->header->string_num ?
        self->original_table[index] : NULL;
}

const char* tinyintl_mofile_get_translated(
    const tinyintl_mofile_t *self, unsigned index)
{
    return index < self->header->string_num ?
        self->translated_table[index] : NULL;
}

static
int search_compare(const char **l, const char **r)
{
    return strcmp(*l, *r);
}

const char* tinyintl_mofile_find_translated(
    const tinyintl_mofile_t *self, const char *original)
{
    if(!original) return NULL;

    char **entry = bsearch(
        &original,
        self->original_table,
        self->header->string_num,
        sizeof(self->original_table[0]),
        (int(*)(const void*,const void*)) search_compare);
    if(!entry)
        return NULL;

    int index = entry - self->original_table;
    return self->translated_table[index];
}

unsigned tinyintl_mofile_count_hash(const tinyintl_mofile_t *self)
{
    return self->header->hash_num;
}

uint32_t tinyintl_mofile_get_hash(const tinyintl_mofile_t *self, unsigned index)
{
    return index < self->header->hash_num ? self->hash_table[index] : 0;
}
