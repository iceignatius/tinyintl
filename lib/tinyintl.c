#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
#   include <windows.h>
#   include <utfconv/utfconv.h>
#else
#   include <locale.h>
#   include <dirent.h>
#   include <sys/stat.h>
#endif

#include "win_lcid_table.h"
#include "tinyintl_mofile.h"
#include "tinyintl.h"

static char curr_locale[128] = {0};
static char curr_domain[FILENAME_MAX] = {0};
static char curr_resdir[FILENAME_MAX] = {0};
static tinyintl_mofile_t curr_mofile = TINYINTL_MOFILE_INITIALISER;

#ifdef _WIN32
static
int compare_lcid(const struct win_lcid_info *l, const struct win_lcid_info *r)
{
    return l->id - r->id;
}
#endif

static
char* query_system_locale_name(int category)
{
#if defined(__APPLE__)
    FILE *file = popen("osascript -e 'user locale of (get system info)'", "r");
    if(!file) return NULL;

    static char locname[64] = {0};
    size_t len = fread(locname, 1, sizeof(locname), file);
    locname[len] = 0;

    if( len && locname[len-1] == '\n' )
        locname[len-1] = 0;

    pclose(file);
    return locname;
#elif defined(_WIN32)
    struct win_lcid_info key = { GetUserDefaultLCID(), "" };
    struct win_lcid_info *info =
        bsearch(
            &key,
            win_lcid_list,
            win_lcid_num,
            sizeof(win_lcid_list[0]),
            (int(*)(const void*,const void*)) compare_lcid);

    return info ? info->name : NULL;
#else
    return getenv("LANG");
#endif
}

static
char* calc_mo_filename(
    char *buf,
    size_t bufsize,
    const char *dir,
    const char *loc,
    const char *domain,
    bool ignore_territory)
{
    static const char category[] = "LC_MESSAGES";

    char locname[ strlen(loc) + 1 ];
    memcpy(locname, loc, sizeof(locname));

    if(ignore_territory)
    {
        char *pos = strchr(locname, '_');
        if(pos)
            *pos = 0;
    }

    snprintf(buf, bufsize, "%s/%s/%s/%s.mo", dir, locname, category, domain);

    return buf;
}

static
bool reload_curr_mofile(void)
{
    if( !curr_locale[0] || !curr_domain[0] || !curr_resdir[0] )
        return false;

    char filename[FILENAME_MAX];
    calc_mo_filename(
        filename, sizeof(filename), curr_resdir, curr_locale, curr_domain, false);
    if(tinyintl_mofile_load_file(&curr_mofile, filename))
        return true;

    // Ignore the territory then try again.
    calc_mo_filename(
        filename, sizeof(filename), curr_resdir, curr_locale, curr_domain, true);
    if(tinyintl_mofile_load_file(&curr_mofile, filename))
        return true;

    return false;
}

char* tinyintl_setlocale(int category, const char *locale)
{
    if(!locale)
    {
        // Nothing to do.
    }
    else if(locale[0])
    {
        strncpy(curr_locale, locale, sizeof(curr_locale) - 1);
    }
    else
    {
        if( !( locale = query_system_locale_name(category) ) )
            return NULL;

        strncpy(curr_locale, locale, sizeof(curr_locale) - 1);
    }

    if(locale)
    {
#ifndef _WIN32
        setlocale(category, locale);
#endif

        // Remove the encoding part.
        char *pos = strchr(curr_locale, '.');
        if(pos)
            *pos = 0;

        reload_curr_mofile();
    }

    return curr_locale;
}

static
void adjust_path_slash(char *str)
{
    for(char *pos = str; *pos; ++pos)
    {
        if( *pos == '\\' )
            *pos = '/';
    }
}

static
void trim_last_slash(char *str)
{
    size_t len = strlen(str);

    if( len && ( str[len-1] == '/' || str[len-1] == '\\' ) )
        str[len-1] = 0;
}

static
bool is_dir_existed(const char *dir)
{
#ifdef _WIN32
    wchar_t wcsdir[FILENAME_MAX];
    if(!utfconv_utf8_to_wcs(wcsdir, FILENAME_MAX, dir))
        return false;

    WIN32_FIND_DATAW finddata;
    HANDLE hfind = FindFirstFileW(wcsdir, &finddata);
    if( hfind == INVALID_HANDLE_VALUE ) return false;
    FindClose(hfind);

    return finddata.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY;
#else
    struct stat state;
    if(stat(dir, &state))
        return false;

    DIR *fdir = opendir(dir);
    if(fdir)
        closedir(fdir);

    return !!fdir;
#endif
}

char* tinyintl_bindtextdomain(const char *domainname, const char *dirname)
{
    if( !domainname || !domainname[0] )
        return NULL;
    if( !dirname || !dirname[0] )
        return NULL;

    char dir[FILENAME_MAX] = {0};
    strncpy(dir, dirname, sizeof(dir) - 1);
    adjust_path_slash(dir);
    trim_last_slash(dir);

    if(!is_dir_existed(dir))
        return NULL;

    strncpy(curr_domain, domainname, sizeof(curr_domain) - 1);
    strncpy(curr_resdir, dir, sizeof(curr_resdir) - 1);

    reload_curr_mofile();

    return curr_resdir;
}

char* tinyintl_textdomain(const char *domainname)
{
    if(!domainname)
        domainname = curr_domain;
    if(!domainname[0])
        return NULL;

    if( curr_domain != domainname )
        strncpy(curr_domain, domainname, sizeof(curr_domain) - 1);

    reload_curr_mofile();

    return curr_domain;
}

char* tinyintl_gettext(const char *msgid)
{
    const char *translated = msgid;
    do
    {
        if(!msgid)
            break;
        if(!tinyintl_mofile_is_loaded(&curr_mofile))
            break;

        const char *str = tinyintl_mofile_find_translated(&curr_mofile, msgid);
        if(!str)
            break;

        translated = str;
    } while(false);

    return (char*) translated;
}
